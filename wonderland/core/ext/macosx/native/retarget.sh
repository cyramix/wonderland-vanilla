#!/bin/bash

#ORIG_PREFIX="/Users/jkaplan/misc/xuggler-latest/xuggle_home/lib"
ORIG_PREFIX="/Users/jkaplan/misc/xuggler-latest/dist/stage/Users/jkaplan/misc/xuggler-latest/xuggle_home/lib"
NEW_PREFIX="@loader_path"

# first, rename relevant jars to .jnilib
mv libxuggle-ferry.4.dylib libxuggle-ferry.jnilib
mv libxuggle-xuggler.dylib libxuggle-xuggler.jnilib
mv libxuggle-xuggler-io.dylib libxuggle-xuggler-io.jnilib

# now update all references to .dylib files to use @loader_path
for lib in *.dylib; do
    for target in *.dylib *.jnilib; do
        echo Change "$ORIG_PREFIX/$lib" to "$NEW_PREFIX/$lib" in $target
        install_name_tool -change "$ORIG_PREFIX/$lib" "$NEW_PREFIX/$lib" $target
    done
done

# finally, deal with references from xuggler and xuggler-io to ferry
install_name_tool -change "$ORIG_PREFIX/libxuggle-ferry.4.dylib" "$NEW_PREFIX/libxuggle-ferry.jnilib" libxuggle-xuggler.jnilib
install_name_tool -change "$ORIG_PREFIX/libxuggle-ferry.4.dylib" "$NEW_PREFIX/libxuggle-ferry.jnilib" libxuggle-xuggler-io.jnilib
